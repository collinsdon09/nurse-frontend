import React from "react";
import SignatureCanvas from "react-signature-canvas";

const SignatureCapture2 = ({ onSaveSignature }) => {
  const sigCanvasRef = React.useRef(null);

  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  const saveSignature = () => {
    if (sigCanvasRef.current.isEmpty()) {
      alert("Please provide your signature.");
      return;
    }
    const signatureImageURL = sigCanvasRef.current.toDataURL();
    onSaveSignature(signatureImageURL); // Pass the URL to the parent component
  };

  return (
    <div>
      <h2>Signature Capture</h2>
      <div>

        
        <SignatureCanvas
          ref={sigCanvasRef}
          penColor="black"
          canvasProps={{
            width: 500,
            height: 200,
            className: "signature-canvas",
          }}
        />
      </div>
      <div>
        <button onClick={clearSignature}>Clear</button>
        <button onClick={saveSignature}>Save Signature</button>
      </div>
    </div>
  );
};

export default SignatureCapture2;
