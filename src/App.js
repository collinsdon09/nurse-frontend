// import logo from "./logo.svg";
import "./App.css";
import SignatureCapture from "./components/Sigpad";
import ImageGallery from "./components/DisplayImage";
import ImageDisplay from "./components/ImageDisplay";
import ImageRenderer from "./components/ImageRenderer";
import SignatureCapture2 from "./components/Sigpad2";
import NurseSearch from "./components/SearchComponents";
import { BrowserRouter, Routes, Route, Link, NavLink } from "react-router-dom";
import EditNurseForm from "./components/EditNurseInfo";
import { ChakraProvider } from '@chakra-ui/react'
import RegistrationFormFinal from "./components/Registration";
import chakraTheme from '@chakra-ui/theme'
import EditFormFinal from "./components/Edit_new";




function App() {


  return (

    <ChakraProvider theme={chakraTheme}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<NurseSearch />} />
        {/* <Route path="/new-nurse" element={<SignatureCapture />} /> */}
        <Route path="/new-nurse" element={<RegistrationFormFinal />} />

        {/* <Route path="/edit-nurse" element={<EditNurseForm />} /> */}
        <Route path="/edit-nurse2" element={<EditFormFinal />} />

      </Routes>
    </BrowserRouter>
    </ChakraProvider>

  );
}

export default App;
